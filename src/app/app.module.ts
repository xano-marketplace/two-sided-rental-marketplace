import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {HomeComponent} from './home/home.component';
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {AuthPanelComponent} from './auth-panel/auth-panel.component';
import {HostHomePanelComponent} from './host-home-panel/host-home-panel.component';
import {SafeHtmlPipe} from "./_demo-core/safe-html.pipe";
import {MatCardModule} from "@angular/material/card";
import {MatInputModule} from "@angular/material/input";
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from "@angular/material/dialog";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from "@angular/material/snack-bar";
import {MatCommonModule, MatNativeDateModule} from "@angular/material/core";
import {ListingsComponent} from './listings/listings.component';
import {ListingComponent} from './listings/listing/listing.component';
import {DemoLandingComponent} from './_demo-core/demo-landing/demo-landing.component';
import {MatDividerModule} from "@angular/material/divider";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatMenuModule} from "@angular/material/menu";
import {AppRouterOutletComponent} from './app-router-outlet/app-router-outlet.component';
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {FiltersPanelComponent} from './filters-panel/filters-panel.component';
import {MatSelectModule} from "@angular/material/select";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {ReservationsPanelComponent} from './reservations-panel/reservations-panel.component';
import {MyListingsComponent} from './my-listings/my-listings.component';
import {MyReservationsComponent} from './my-reservations/my-reservations.component';
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatChipsModule} from "@angular/material/chips";

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		HeaderComponent,
		AuthPanelComponent,
		HostHomePanelComponent,
		SafeHtmlPipe,
		ListingsComponent,
		ListingComponent,
		DemoLandingComponent,
		AppRouterOutletComponent,
		FiltersPanelComponent,
		ReservationsPanelComponent,
		MyListingsComponent,
		MyReservationsComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		MatCommonModule,
		HttpClientModule,
		BrowserAnimationsModule,
		ReactiveFormsModule,
		MatToolbarModule,
		MatIconModule,
		MatButtonModule,
		MatSnackBarModule,
		MatCardModule,
		MatInputModule,
		MatDialogModule,
		MatToolbarModule,
		MatDividerModule,
		MatExpansionModule,
		MatMenuModule,
		MatAutocompleteModule,
		MatCheckboxModule,
		MatSelectModule,
		MatProgressSpinnerModule,
		MatNativeDateModule,
		MatDatepickerModule,
		MatTableModule,
		MatPaginatorModule,
		MatChipsModule
	],
	providers: [{
		provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
		useValue: {
			duration: 4000,
			horizontalPosition: 'start'
		}
	}, {
		provide: MAT_DIALOG_DEFAULT_OPTIONS,
		useValue: {
			hasBackdrop: true,
			minHeight: '100vh',
			position: {top: '0px', right: '0px'},
			panelClass: 'slide-out-panel'
		}
	}],
	bootstrap: [AppComponent]
})
export class AppModule {
}
