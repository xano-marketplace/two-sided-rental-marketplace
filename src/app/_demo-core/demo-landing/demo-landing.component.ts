import {Component, OnInit, ViewChild} from '@angular/core';
import {ConfigService, XanoConfig} from "../config.service";
import {MatAccordion} from "@angular/material/expansion";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router'

@Component({
	selector: 'app-demo-landing',
	templateUrl: './demo-landing.component.html',
	styleUrls: ['./demo-landing.component.scss']
})
export class DemoLandingComponent implements OnInit {
	public config: XanoConfig;
	@ViewChild(MatAccordion) accordion: MatAccordion;
	public configForm: FormGroup = new FormGroup({
		xanoUrl: new FormControl(this.configService.xanoApiUrl.value,
			[Validators.required, Validators.pattern('^https?://.+')]
		)
	})

	constructor(
		private configService: ConfigService,
		private router: Router,
		private route: ActivatedRoute
	) {
		this.config = configService.config;
	}

	ngOnInit(): void {
		this.route.queryParams.subscribe(res => {
			if(res?.api_url) {
				this.configForm.controls.xanoUrl.patchValue(res.api_url)
				this.pasteSubmit();
			}
		});
	}

	public submit(): void {
		this.configForm.markAllAsTouched();
		if (this.configForm.valid) {
			this.configService.configGet(this.configForm.controls.xanoUrl.value)
				.subscribe(res => {
						if (!this.configService.config.requiredApiPaths.every(path => res.includes(path))) {
							const message = 'This Xano Base URL is missing the required endpoints. Have you installed this marketplace extension?';
							this.configService.showErrorSnack({error: {message: message}})
						} else {
							this.configService.xanoApiUrl.next(this.configForm.controls.xanoUrl.value);
							this.router.navigate(['home'])
						}
					}, error => this.configService.showErrorSnack(error)
				);

		}
	}

	public pasteSubmit() {
		setTimeout(() => {
			this.submit();
		}, 100)
	}
}
