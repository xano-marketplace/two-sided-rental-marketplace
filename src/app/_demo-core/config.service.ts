import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {XanoService} from "./xano.service";
import {ApiService} from "./api.service";
import {get} from 'lodash-es';
import {MatSnackBar} from "@angular/material/snack-bar";

export interface XanoConfig {
	title: string,
	summary: string,
	editText: string,
	editLink: string,
	descriptionHtml: string,
	components: any,
	instructions: any;
	logoHtml: string,
	requiredApiPaths: string[]
}

export interface Arg {
	tag?: string,
	value?: any
}

export interface Filters {
	name?: string,
	arg?: Arg[]
}

export interface Left {
	operand: string,
	tag: string,
	filters?: Filters[]
}

export interface Right {
	operand: string,
	tag: string,
	filters?: Filters[],
	ignore_empty?: boolean
}

export interface Expression {
	operator?: string,
	left: Left,
	right?: Right
	or?: boolean
}

export interface ColumnSearchExpression {
	operator?: string,
	column: string,
	query: string | boolean,
	or?: boolean
}

export interface GeoSearchExpression {
	column: string,
	lng: number,
	lat: number,
	radius: number,
	or?: boolean
}

@Injectable({
	providedIn: 'root'
})


export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public authToken: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public user: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public searchQuery: BehaviorSubject<any> = new BehaviorSubject<any>(null);

	public config: XanoConfig = {
		title: 'Rental Marketplace',
		summary: 'Demo of a two-sided rental marketplace.',
		editText: 'Get source code',
		editLink: 'https://gitlab.com/xano-marketplace/two-sided-rental-marketplace',
		components: [
			{
				name: 'Home',
				description: 'A basic landing page including quick links to search listings. The "Find a Stay" tiles ' +
					'use geolocation to search your area for available types of places. If no results come up, it is ' +
					'because the included data is too limited. You can add a listing in your area (if available) and ' +
					'then try again. The "Explore a Location" tiles will show listings in a specific location.'
			},
			{
				name: 'Listings',
				description: 'This is where you can see search results for listings. Advanced filtering capabilities are ' +
					'provided to narrow a search. Check-in and check-out dates will filter out unavailable listings if ' +
					'they are both booked and confirmed by a host.'
			},
			{
				name: 'Listing',
				description: 'Here you can view a single listing in more detail and reserve it.'
			},
			{
				name: 'My Reservations',
				description: 'You can view your reservations, see their status, and even cancel them. To access this ' +
					'page go to the top right button and access the drop-down item "My Reservations."'
			},
			{
				name: 'My Listings',
				description: 'This is where you can view and edit your listings, as well as approve, deny, or cancel ' +
					'reservation requests. To access this page go to the top right button and access the drop-down ' +
					'item "My Listings."'
			},
			{
				name: 'Authentication',
				description: 'Here you can create a basic profile and authenticate into the application. To access this ' +
					'panel go to the top right button and access the drop-down items "Signup" or "Login."'
			},
			{
				name: 'Host Home Management',
				description: 'This panel is a multi-step form for adding, modifying, or deleting listings. To access ' +
					'this page go to the top right button and access the drop-down item "Host Home" or ' +
					'through the "My Listings" page table.'
			},
			{
				name: 'Filter Panel',
				description: 'This panel allows you to center in on just what you are looking for in a listing ' +
					'through filters. To access this panel, under the listings page, click "More Filters."'
			},
		],
		instructions: [
			'Install the template in your Xano Workspace',
			'Go to the newly added Rental Marketplace API Group and copy your API BASE URL',
			'In this demo, paste this API BASE URL in as "Your Xano API URL"'
		],
		descriptionHtml: ``,
		logoHtml: '',
		requiredApiPaths: [
			'/auth/login',
			'/auth/signup',
			'/listings',
			'/listings/{listings_id}',
			'/reservations',
			'/reservations/{reservations_id}',
			'/upload/image'
		]
	};

	constructor(
		private apiService: ApiService,
		private xanoService: XanoService,
		private snackBar: MatSnackBar) {
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public isLoggedIn(): Observable<any> {
		return this.authToken.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(apiUrl),
			headers: {
				Accept: 'text/yaml'
			},
			responseType: 'text',
		});
	}

	public showErrorSnack(error) {
		this.snackBar.open(get(error, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
	}

	public searchExpressionBuilder(expressions: Expression[], asGroup: boolean, orGroup: boolean) {
		const statements = [];

		for (let expression of expressions) {
			let statement = {
				statement: {
					left: {
						tag: expression.left.tag,
						operand: expression.left.operand
					},
					right: {
						operand: expression.right.operand
					}
				}
			}

			if (expression.left.filters?.length) {
				Object.assign(statement.statement.left, {filters: expression.left.filters})
			}

			if (expression.operator) {
				Object.assign(statement.statement, {op: expression.operator})
			}

			if (statements.length && expression.or) {
				Object.assign(statement, {or: true})
			}

			if (expression.right.tag) {
				Object.assign(statement.statement.right, {tag: expression.right.tag})
			}

			if (expression.right.filters?.length) {
				Object.assign(statement.statement.right, {filters: expression.right.filters})
			}

			if (expression.right.ignore_empty) {
				Object.assign(statement.statement.right, {ignore_empty: expression.right.ignore_empty})
			}

			statements.push(statement)
		}

		if (asGroup) {
			return {
				type: 'group',
				or: orGroup,
				group: {
					expression: statements
				}
			}
		} else {
			return statements;
		}
	}


	public searchColumn(columnSearchExpressions: ColumnSearchExpression[], asGroup, orGroup) {
		let expressions = [];

		for (let expression of columnSearchExpressions) {
			expressions.push({
				left: {
					tag: 'col',
					operand: expression.column
				},
				operator: expression.operator,
				right: {
					operand: expression.query
				},
				or: expression.or
			});
		}
		return this.searchExpressionBuilder(expressions, asGroup, orGroup);
	}

	public geoLocationSearch(geoSearchExpressions: GeoSearchExpression[], asGroup, orGroup) {
		let expressions = []

		for (let expression of geoSearchExpressions) {
			expressions.push({
				or: expression.or,
				left: {
					tag: 'col',
					operand: expression.column,
					filters: [{
						name: 'within',
						arg: [
							{
								tag: 'const',
								value: {
									type: 'point',
									data: {
										lat: expression.lat,
										lng: expression.lng
									}
								}
							}, {
								tag: 'const',
								value: expression.radius
							}
						]
					},
					]
				},
				right: {
					operand: true
				}
			});
		}
		return this.searchExpressionBuilder(expressions, asGroup, orGroup);
	}

}

