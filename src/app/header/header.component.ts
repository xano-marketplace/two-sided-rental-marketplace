import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../_demo-core/config.service';
import {MatDialog} from '@angular/material/dialog';
import {AuthPanelComponent} from "../auth-panel/auth-panel.component";
import {HostHomePanelComponent} from "../host-home-panel/host-home-panel.component";
import {cities} from "../_demo-core/cities";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs";
import {map, startWith} from "rxjs/operators";
import {Router} from "@angular/router";

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
	public apiConfigured: boolean = false;
	public config: XanoConfig;
	public searchForm: FormGroup = new FormGroup({
		location: new FormControl('', Validators.required),
		start_at: new FormControl(),
		end_at: new FormControl()
	})
	public locationControl = new FormControl();
	public filteredLocations: Observable<any[]>;
	public locations = cities;
	public point: any;
	public user;
	public today: Date =  new Date();
	public tomorrow: Date = new Date();

	constructor(
		private configService: ConfigService,
		private dialog: MatDialog,
		private router: Router
	) {
		this.filteredLocations = this.searchForm.controls.location.valueChanges
			.pipe(
				startWith(''),
				map(value => value ? this.filterLocations(value) : this.locations.slice())
			);
	}

	ngOnInit(): void {
		this.tomorrow.setDate(this.tomorrow.getDate() + 1)
		this.config = this.configService.config;
		this.configService.isConfigured().subscribe(apiUrl => {
			this.apiConfigured = !!apiUrl;
		});

		this.configService.user.asObservable().subscribe(res => {
			console.log(res)
			this.user = res
		});
	}

	public showPanel(dialogType, signup): void {
		let dialogRef;
		switch (dialogType) {
			case 'auth':
				dialogRef = this.dialog.open(AuthPanelComponent, {data: {signup: signup}});
				break;
			case 'manage':
				dialogRef = this.dialog.open(HostHomePanelComponent, {minWidth: '50vw'});
				break;
			default:
				break;
		}
	}

	public searchLocation(location, initiateSearch: boolean = false): any {
		this.point = {
			type: "point",
			data: {
				lat: location.latitude,
				lng: location.longitude
			}
		}

		if(initiateSearch) {
			this.search();
		}

	}

	public search() {
		if(this.searchForm.valid) {
			const startDate = this.searchForm.controls.start_at.value;
			const endDate = this.searchForm.controls.end_at.value;
			if(startDate && endDate && startDate >= endDate) {
				endDate.setDate(startDate.getDate() + 1)
				this.searchForm.controls.end_at.patchValue(endDate)
			}
			this.router.navigate(['/listings']).then(x => this.configService.searchQuery.next({
				point: this.point,
				start_at: this.searchForm.controls.start_at.value,
				end_at: this.searchForm.controls.end_at.value
			}));
		}
	}

	public openHostHomePanel() {
		this.dialog.open(HostHomePanelComponent, {minWidth: "30vw"})
	}

	private filterLocations(value: string): any[] {
		const filterValue = value.toLowerCase();

		return this.locations.filter(x => x.city.toLowerCase().indexOf(filterValue) === 0);
	}

	public logout() {
		this.configService.authToken.next(null);
		this.configService.user.next(null);
	}
}
