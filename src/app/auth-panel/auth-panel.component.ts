import {Component, Inject, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../_demo-core/config.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../api-services/auth.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ListingsService} from "../api-services/listings.service";
import {finalize} from "rxjs/operators";
import {UploadService} from "../api-services/upload.service";

@Component({
	selector: 'app-auth-panel',
	templateUrl: './auth-panel.component.html',
	styleUrls: ['./auth-panel.component.scss']
})
export class AuthPanelComponent implements OnInit {
	public config: XanoConfig;
	public showLoginView: boolean = !this.data.signup;
	public uploading: boolean = false;
	public image: any;
	private emailRegex = new RegExp('^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$');

	public loginForm: FormGroup = new FormGroup({
		email: new FormControl('', [Validators.required, Validators.pattern(this.emailRegex)]),
		password: new FormControl('', Validators.required)
	});

	public signupForm: FormGroup = new FormGroup({
		profile_image: new FormControl(null),
		name: new FormControl('', [Validators.required]),
		email: new FormControl('', [Validators.required, Validators.pattern(this.emailRegex)]),
		password: new FormControl('', Validators.required)
	});

	constructor(
		private configService: ConfigService,
		private authService: AuthService,
		private uploadService: UploadService,
		private dialogRef: MatDialogRef<AuthPanelComponent>,
		private listingService: ListingsService,
		private snackBar: MatSnackBar,
		@Inject(MAT_DIALOG_DATA) public data
	) {
	}

	ngOnInit(): void {
		this.config = this.configService.config;
	}

	public submit(): void {
		if (this.showLoginView) {
			this.loginForm.markAllAsTouched();

			if (this.loginForm.valid) {
				this.authService.login(this.loginForm.getRawValue()).subscribe(authToken => {
					if (authToken) {
						this.configService.authToken.next(authToken.authToken);
						this.authService.userGet().subscribe(res => this.configService.user.next(res));
						this.dialogRef.close();
					} else {
						this.snackBar.open('No Auth Token', 'Error', {panelClass: 'error-snack'});
					}
				}, err => {
					this.snackBar.open(err.error.message, 'Error', {panelClass: 'error-snack'});
				});
			}
		} else {
			if (this.signupForm.valid) {
				this.authService.signup(this.signupForm.getRawValue()).subscribe(authToken => {
					if (authToken) {
						this.configService.authToken.next(authToken.authToken);
						this.authService.userGet().subscribe(res => this.configService.user.next(res));
						this.dialogRef.close();
					} else {
						this.snackBar.open('No Auth Token', 'Error', {panelClass: 'error-snack'});
					}
				}, err => {
					this.snackBar.open(err.error.message, 'Error', {panelClass: 'error-snack'});
				});
			}
		}
	}

	public upload(event): void {
		this.uploading = true;
		const file: File = event.target.files[0];
		const formData: FormData = new FormData();
		formData.append('content', file, file.name);
		this.uploadService.upload(formData)
			.pipe(
				finalize(() => this.uploading = false)
			)
			.subscribe(res => {
				this.signupForm.controls.profile_image.patchValue(res);
				const reader = new FileReader();
				reader.onload = () => this.image = reader.result;
				reader.readAsDataURL(file);
			}, error => this.snackBar.open('Image Upload', 'Failed', {panelClass: 'error-snack'}));
	}

	public deleteImage() {
		this.signupForm.controls.profile_image.patchValue(null);
	}


}
