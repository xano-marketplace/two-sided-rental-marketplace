import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {DemoLandingComponent} from "./_demo-core/demo-landing/demo-landing.component";
import {ConfigGuard} from "./_demo-core/config.guard";
import {ListingsComponent} from "./listings/listings.component";
import {AppRouterOutletComponent} from "./app-router-outlet/app-router-outlet.component";
import {ListingComponent} from "./listings/listing/listing.component";
import {MyListingsComponent} from "./my-listings/my-listings.component";
import {MyReservationsComponent} from "./my-reservations/my-reservations.component";
import {AuthGuard} from "./auth.guard";

const routes: Routes = [
	{path: '', component: DemoLandingComponent, data: {animation: 'demoLandingPage'}},
	{path: 'home', component: HomeComponent, canActivate: [ConfigGuard]},
	{path: 'listings', component: ListingsComponent, canActivate: [ConfigGuard]},
	{path: 'listings/listing/:id', component: ListingComponent, canActivate: [ConfigGuard]},
	{path: 'my-listings', component: MyListingsComponent,  canActivate: [ConfigGuard, AuthGuard]},
	{path: 'my-reservations', component: MyReservationsComponent, canActivate: [ConfigGuard, AuthGuard]}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
