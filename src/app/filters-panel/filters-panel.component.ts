import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormControl, FormGroup} from "@angular/forms";
import {identity, pickBy} from 'lodash-es';

@Component({
	selector: 'app-filters-panel',
	templateUrl: './filters-panel.component.html',
	styleUrls: ['./filters-panel.component.scss']
})
export class FiltersPanelComponent implements OnInit {

	public amenitiesForm: FormGroup = new FormGroup({
		kitchen: new FormControl(null),
		heating: new FormControl(null),
		washer: new FormControl(null),
		wifi: new FormControl(null),
		fireplace: new FormControl(null),
		workspace: new FormControl(null),
		crib: new FormControl(null),
		self_checkin: new FormControl(null),
		CO_alarm: new FormControl(null),
		piano: new FormControl(null),
		shampoo: new FormControl(null),
		air_conditioning: new FormControl(null),
		dryer: new FormControl(null),
		breakfast: new FormControl(null),
		hangers: new FormControl(null),
		hair_dryer: new FormControl(null),
		tv: new FormControl(null),
		high_chair: new FormControl(null),
		smoke_alarm: new FormControl(null),
		private_bathroom: new FormControl(null),
		waterfront: new FormControl(null)
	});
	public amenitiesFormControls = Object.getOwnPropertyNames(this.amenitiesForm.controls);
	public filtersForm: FormGroup = new FormGroup({
		occupancy: new FormControl(),
		bedrooms: new FormControl(),
		bathrooms: new FormControl(),
		smoking_allowed: new FormControl(),
		pets_allowed: new FormControl()
	});
	public facilitiesForm: FormGroup = new FormGroup({
		free_parking: new FormControl(),
		gym: new FormControl(),
		hot_tub: new FormControl(),
		pool: new FormControl()
	});
	public facilitiesFormControls = Object.getOwnPropertyNames(this.facilitiesForm.controls)

	constructor(
		private dialogRef: MatDialogRef<any>,
		@Inject(MAT_DIALOG_DATA) public data
	) {
	}

	ngOnInit(): void {
		if (this.data?.filters) {
			this.amenitiesForm.patchValue(this.data.filters?.amenities);
			this.facilitiesForm.patchValue(this.data.filters?.facilities);
			this.filtersForm.patchValue(this.data.filters);
		}
	}

	public submit(): void {
		const amenities = this.amenitiesForm.getRawValue();
		const facilities = this.facilitiesForm.getRawValue()

		this.dialogRef.close({
			amenities: pickBy(amenities, identity),
			facilities: pickBy(facilities, identity),
			...pickBy(this.filtersForm.getRawValue(), identity)
		})
	}
}
