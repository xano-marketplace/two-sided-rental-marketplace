import {Component, OnInit} from '@angular/core';
import {ListingsService} from "../api-services/listings.service";
import {ConfigService} from "../_demo-core/config.service";
import {MatDialog} from "@angular/material/dialog";
import {HostHomePanelComponent} from "../host-home-panel/host-home-panel.component";
import {finalize} from "rxjs/operators";

@Component({
	selector: 'app-my-listings',
	templateUrl: './my-listings.component.html',
	styleUrls: ['./my-listings.component.scss']
})
export class MyListingsComponent implements OnInit {

	constructor(
		private listingsService: ListingsService,
		private configService: ConfigService,
		private dialog: MatDialog
	) {
	}

	public listings = [];
	public reservations = [];
	public loading: boolean;
	public today = new Date();

	public listingsTable = {
		noResults: false,
		displayedColumns: ['id', 'name', 'property_type', 'location', 'edit'],
		currentPage: undefined,
		totalItems: undefined
	}

	public reservationsTable = {
		noResults: false,
		displayedColumns: ['id', 'request_from', 'property_name', 'dates', 'pricing', 'approve', 'cancel'],
		currentPage: undefined,
		totalItems: undefined
	}

	ngOnInit(): void {
		this.getListings(null);
		this.getReservations(null);
	}

	public approve(reservationID, approve, deny, canceled): void {
		this.listingsService.myListingsReservationsApprove(reservationID, approve, deny, canceled).subscribe(res => {
			if(res) {
				this.reservations.map((x) => {
					if(x.id === reservationID) {
						x.confirmed_at = res.confirmed_at;
						x.denied_at = res.denied_at
						x.canceled_at = res.canceled_at
					}
					return x;
				})
			}
		}, error => this.configService.showErrorSnack(error))
	}

	public editListing(listing): void {
		this.dialog.open(HostHomePanelComponent, {data: {listing}})
	}

	public getListings(search) {
		this.listingsService.myListingsGet(search)
			.pipe(finalize(()=> this.listingsTable.noResults = !this.listings?.length))
			.subscribe(res => {
				this.listings = res?.items;
				this.listingsTable.totalItems = res?.itemsTotal;
				this.listingsTable.currentPage = res?.currentPage;
			}, error => this.configService.showErrorSnack(error))
	}

	public getReservations(search) {
		this.listingsService.myListingsReservationsGet(search)
			.pipe(finalize(() => this.reservationsTable.noResults = !this.reservations?.length))
			.subscribe(res => {
				this.reservations = res?.items;
				this.reservationsTable.totalItems = res?.itemsTotal;
				this.reservationsTable.currentPage = res?.currentPage;
			}, error => this.configService.showErrorSnack(error))
	}

	public pageChangeEvent(event, type): void {
		if(type === 'reservations') {
			this.getReservations( {page: event.pageIndex + 1})
		} else {
			this.getListings({page: event.pageIndex + 1})
		}
	}

}
