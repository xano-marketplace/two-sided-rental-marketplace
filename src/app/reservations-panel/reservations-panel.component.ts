import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ReservationsService} from "../api-services/reservations.service";
import {ConfigService} from "../_demo-core/config.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {AuthPanelComponent} from "../auth-panel/auth-panel.component";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
	selector: 'app-reservations-panel',
	templateUrl: './reservations-panel.component.html',
	styleUrls: ['./reservations-panel.component.scss']
})
export class ReservationsPanelComponent implements OnInit {

	public notLoggedIn: boolean;
	public numberOfDays: number;
	public start_at: any;
	public reservationsForm: FormGroup = new FormGroup({
		id: new FormControl(),
		user_id: new FormControl('', Validators.required),
		listings_id: new FormControl('', Validators.required),
		start_at: new FormControl('', Validators.required),
		end_at: new FormControl('', Validators.required),
		price: new FormControl('', Validators.required),
		booking_fee: new FormControl('', Validators.required),
		total: new FormControl('', Validators.required),
		confirmation_date: new FormControl()
	});

	constructor(
		private reservationsService: ReservationsService,
		private configService: ConfigService,
		private dialog: MatDialog,
		private dialogRef: MatDialogRef<ReservationsPanelComponent>,
		private snackbar: MatSnackBar,
		@Inject(MAT_DIALOG_DATA) public data
	) {
	}

	ngOnInit(): void {
		if (this.data?.listing) {
			this.reservationsForm.controls.listings_id.patchValue(this.data.listing.id)
			this.reservationsForm.controls.price.patchValue(this.data.listing.price)
		}

		if (this.data?.dates) {
			this.reservationsForm.patchValue(this.data?.dates)
			this.numberOfDays = Math.floor((Date.parse(this.data?.dates?.end_at) - Date.parse(this.data?.dates?.start_at)) / 86400000);

			const priceBeforeFee = this.data?.listing?.price * this.numberOfDays;
			const fee = priceBeforeFee * 10 / 100;
			this.reservationsForm.controls.booking_fee.patchValue(fee);
			this.reservationsForm.controls.total.patchValue(priceBeforeFee + fee)
		}


		this.configService.user.asObservable().subscribe(res => {


			if (!res?.id) {
				this.notLoggedIn = true
				this.dialog.open(AuthPanelComponent, {data: {signup: false}})
			} else {
				this.notLoggedIn = false;
				this.reservationsForm.controls.user_id.patchValue(res?.id)
			}
		});
	}

	public submit() {
		if (this.reservationsForm.valid) {
			this.reservationsService.reservationsSave(this.reservationsForm.getRawValue()).subscribe(res => {
				if (res) {
					this.snackbar.open('Reservation', 'Requested')
					this.dialogRef.close()
				}
			}, error => this.configService.showErrorSnack(error))
		}
	}

	public login() {
		this.dialog.open(AuthPanelComponent, {data: {signup: false}})
	}
}
