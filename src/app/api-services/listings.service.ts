import {Injectable} from '@angular/core';
import {ConfigService} from "../_demo-core/config.service";
import {ApiService} from "../_demo-core/api.service";
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class ListingsService {

	constructor(
		private configService: ConfigService,
		private apiService: ApiService
	) {
	}

	public listingsSave(listings): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/listings`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: listings,
		});
	}


	public listingGet(listingsID): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/listings/${listingsID}`,
		});
	}

	public listingsGet(search, start_at = null , end_at = null): Observable<any> {
		let params = {
			search
		}
		if(start_at) {
			params['start_at'] =  start_at;
		}
		if(end_at) {
			params['end_at'] =  end_at;
		}

		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/listings`,
			params: params
		});
	}


	public listingsDelete(listingsId): Observable<any> {
		return this.apiService.delete({
			endpoint: `${this.configService.xanoApiUrl.value}/listings/${listingsId}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
		});
	}

	public listingsUpdate(listings): Observable<any> {
		console.log(listings)
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/listings/${listings.id}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: listings
		});
	}

	public myListingsGet(search): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/my/listings`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: {search}
		});
	}

	public myListingsReservationsGet(search): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/my/listings/reservations`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: {search}
		});
	}


	public myListingsReservationsApprove(reservations_id, approve, deny, canceled): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/my/listings/reservations/${reservations_id}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: {
				confirmed: approve,
				denied: deny,
				canceled: canceled
			}
		});
	}
}
