import {Injectable} from '@angular/core';
import {ConfigService} from "../_demo-core/config.service";
import {ApiService} from "../_demo-core/api.service";
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	constructor(
		private configService: ConfigService,
		private apiService: ApiService
	) {
	}

	public login(login: { email: string, password: string }): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/auth/login`,
			params: {
				email: login.email,
				password: login.password
			}
		});
	}

	public signup(signup): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/auth/signup`,
			params: signup
		});
	}

	public userGet(): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/auth/me`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`}
		});
	}

}
