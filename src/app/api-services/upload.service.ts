import {Injectable} from '@angular/core';
import {ConfigService} from "../_demo-core/config.service";
import {ApiService} from "../_demo-core/api.service";
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class UploadService {

	constructor(
		private configService: ConfigService,
		private apiService: ApiService
	) {
	}

	public upload(image): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/upload/image`,
			// headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: image
		});
	}
}
