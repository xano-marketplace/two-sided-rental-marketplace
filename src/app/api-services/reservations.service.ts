import {Injectable} from '@angular/core';
import {ConfigService} from "../_demo-core/config.service";
import {ApiService} from "../_demo-core/api.service";
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class ReservationsService {

	constructor(
		private configService: ConfigService,
		private apiService: ApiService
	) {
	}

	public reservationsSave(reservations): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/reservations`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: reservations,
		});
	}

	public reservationsGet(reservationsID): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/reservations/${reservationsID}`,
		});
	}

	public reservationsDelete(reservationsId): Observable<any> {
		return this.apiService.delete({
			endpoint: `${this.configService.xanoApiUrl.value}/reservations/${reservationsId}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
		});
	}

	public reservationsUpdate(reservations): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/reservations/${reservations.id}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: reservations
		});
	}

	public myReservationsGet(search): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/my/reservations`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: {search}
		});
	}
}
