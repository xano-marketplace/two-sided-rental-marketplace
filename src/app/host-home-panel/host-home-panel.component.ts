import {Component, Inject, inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {cities} from "../_demo-core/cities";
import {Observable} from "rxjs";
import {finalize, map, startWith} from "rxjs/operators";
import {ListingsService} from "../api-services/listings.service";
import {UploadService} from "../api-services/upload.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ConfigService} from "../_demo-core/config.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
	selector: 'app-host-home-panel',
	templateUrl: './host-home-panel.component.html',
	styleUrls: ['./host-home-panel.component.scss']
})
export class HostHomePanelComponent implements OnInit {

	public step: number = 0;
	public cities = cities;
	public listingsForm: FormGroup = new FormGroup({
		id: new FormControl(null),
		property_type: new FormControl('house', Validators.required),
		place_type: new FormControl('private_room', Validators.required),
		occupancy: new FormControl('', Validators.required),
		bedrooms: new FormControl('', Validators.required),
		bathrooms: new FormControl('', Validators.required),
		smoking_allowed: new FormControl(true),
		pets_allowed: new FormControl(true),
		owner_id: new FormControl(null, Validators.required),
		price: new FormControl(null, Validators.required),
		images: new FormControl([]),
		name: new FormControl('', Validators.required),
		description: new FormControl('', Validators.required),
		gps_coordinates: new FormControl(),
		address: new FormGroup({
			address_line1: new FormControl(),
			address_line2: new FormControl(),
			city: new FormControl('', Validators.required),
			state: new FormControl(),
			country: new FormControl(),
			postal_code: new FormControl()
		}),
		amenities: new FormGroup({
			kitchen: new FormControl(),
			heating: new FormControl(),
			washer: new FormControl(),
			wifi: new FormControl(),
			fireplace: new FormControl(),
			workspace: new FormControl(),
			crib: new FormControl(),
			self_checkin: new FormControl(),
			CO_alarm: new FormControl(),
			piano: new FormControl(),
			shampoo: new FormControl(),
			air_conditioning: new FormControl(),
			dryer: new FormControl(),
			breakfast: new FormControl(),
			hangers: new FormControl(),
			hair_dryer: new FormControl(),
			tv: new FormControl(),
			high_chair: new FormControl(),
			smoke_alarm: new FormControl(),
			private_bathroom: new FormControl(),
			waterfront: new FormControl()
		}),
		facilities: new FormGroup({
			free_parking: new FormControl(),
			gym: new FormControl(),
			hot_tub: new FormControl(),
			pool: new FormControl()
		}),
		published_at: new FormControl(new Date())
	});
	public filteredLocations: Observable<any[]>;
	public locations = cities;
	public filteredStateLocations: Observable<any[]>;
	// @ts-ignore
	public amenitiesFormControls = Object.getOwnPropertyNames(this.listingsForm.controls.amenities.controls);
	// @ts-ignore
	public facilitiesFormControls = Object.getOwnPropertyNames(this.listingsForm.controls.facilities.controls);
	public uploading: boolean;
	public images = [];

	constructor(
		private configService: ConfigService,
		private listingsService: ListingsService,
		private uploadService: UploadService,
		private dialogRef: MatDialogRef<HostHomePanelComponent>,
		private snackBar: MatSnackBar,
		@Inject(MAT_DIALOG_DATA) public data
	) {
		// @ts-ignore

		this.filteredLocations = this.listingsForm.controls.address.controls.city.valueChanges
			.pipe(
				startWith(''),
				map(value => value ? this.filterLocations(value) : this.locations.slice())
			);

		// @ts-ignore
		this.filteredStateLocations = this.listingsForm.controls.address.controls.state.valueChanges
			.pipe(
				startWith(''),
				map(value => value ? this.filterLocations(value) : this.locations.slice())
			);
	}

	ngOnInit(): void {
		if(this.data?.listing) {
			this.listingsForm.patchValue(this.data?.listing);
			this.images = this.data?.listing?.images;
		}
	}

	public submit(): void {
		this.configService.user.asObservable().subscribe(res => this.listingsForm.controls.owner_id.patchValue(res?.id))

		this.listingsForm.markAllAsTouched();
		if (this.listingsForm.valid) {
			if(!this.data?.listing) {
				this.listingsService.listingsSave(this.listingsForm.getRawValue()).subscribe(res => {
					this.snackBar.open('Listing', 'Saved');
					this.dialogRef.close()
				}, error => this.configService.showErrorSnack(error));
			} else {
				this.listingsService.listingsUpdate(this.listingsForm.getRawValue()).subscribe(res => {
					this.snackBar.open('Listing', 'Updated');
					this.dialogRef.close()
				}, error => this.configService.showErrorSnack(error));
			}
		} else {
			this.snackBar.open('Please Fix Form', 'Error', {panelClass: 'error-snack'});
		}
	}

	public gpsLocation(location): any {
		const point = {
			type: "point",
			data: {
				lat: location.latitude,
				lng: location.longitude
			}
		}
		this.listingsForm.controls.gps_coordinates.patchValue(point);
	}

	public upload(event): void {
		this.uploading = true;
		const file: File = event.target.files[0];
		const formData: FormData = new FormData();
		formData.append('content', file, file.name);
		this.uploadService.upload(formData)
			.pipe(
				finalize(() => this.uploading = false)
			)
			.subscribe(res => {
				this.listingsForm.controls.images.patchValue([...this.listingsForm.controls.images.value, res]);
				const reader = new FileReader();
				reader.onload = () => this.images = [...this.images, reader.result];
				reader.readAsDataURL(file);
			}, error => this.snackBar.open('Image Upload', 'Failed', {panelClass: 'error-snack'}));
	}

	public deleteImage(image) {
		let index = this.images.indexOf(image);
		this.listingsForm.controls.images.value.splice(index, 1);
		this.listingsForm.controls.images.patchValue(this.listingsForm.controls.images.value);
		this.images = this.images.filter(x => x !== image);
	}

	private filterLocations(value: any): any[] {
		const filterValue = value.toLowerCase();

		return this.locations.filter(x => x.city.toLowerCase().indexOf(filterValue) === 0);
	}

	public deleteListing(): void {
		if(this.data?.listing){
			this.listingsService.listingsDelete(this.data.listing.id).subscribe(res =>  {
				this.dialogRef.close({deleted: true, item: this.data.listing});
			}, error => this.configService.showErrorSnack(error))
		}
	}
}
