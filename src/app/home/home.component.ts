import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from "../_demo-core/config.service";
import {cities} from "../_demo-core/cities";
import {Router} from "@angular/router";

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	public config: XanoConfig;
	public cities = cities;
	public configured: boolean = false;

	constructor(
		private configService: ConfigService,
		private router: Router
	) {
	}

	ngOnInit(): void {
		this.config = this.configService.config;
		this.configService.xanoApiUrl.subscribe(apiUrl => this.configured = !!apiUrl)
	}

	public explore(value, type): void {
		this.configService.searchQuery.next(null);
		if (type === 'location') {
			let city = this.cities.find(x => x.city === value);

			let point = {
				type: "point",
				data: {
					lng: city.longitude,
					lat: city.latitude
				}
			}
			this.router.navigate(['/listings']).then(() => {
				this.configService.searchQuery.next({point: point})
			});

		} else if (type === 'type-of-place') {
			this.router.navigate(['/listings'], {queryParams: {type_of_place: value}});
		} else if (type === 'pets-allowed') {
			this.router.navigate(['/listings'], {queryParams: {pets_allowed: value}});
		}
	}
}
