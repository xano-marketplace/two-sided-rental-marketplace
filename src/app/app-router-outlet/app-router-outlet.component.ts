import {Component, OnInit} from '@angular/core';

@Component({
	selector: 'app-app-router-outlet',
	templateUrl: './app-router-outlet.component.html',
	styleUrls: ['./app-router-outlet.component.scss']
})
export class AppRouterOutletComponent implements OnInit {

	constructor() {
	}

	ngOnInit(): void {
	}

}
