import {Component} from '@angular/core';
import {ConfigService} from "./_demo-core/config.service";
import {Title} from "@angular/platform-browser";
import {fadeAnimation} from "./_demo-core/router-animation";
import {NavigationStart, Router, RouterEvent, RouterOutlet} from "@angular/router";
import {filter} from "rxjs/operators";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	animations: [fadeAnimation]
})
export class AppComponent {
	constructor(
		configService: ConfigService,
		public title: Title,
		private router: Router
	) {
		title.setTitle(configService.config.title);
		this.appSummary = configService.config.summary

		this.router.events
			.pipe(filter(event => event instanceof NavigationStart))
			.subscribe((event: RouterEvent) => {
				this.isDemoLandingPage = event.url === '/'
			});
	}
	public appSummary: string = '';
	public isDemoLandingPage: boolean = true;
}
