import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ColumnSearchExpression, ConfigService, GeoSearchExpression} from "../_demo-core/config.service";
import {MatDialog} from "@angular/material/dialog";
import {FiltersPanelComponent} from "../filters-panel/filters-panel.component";
import {cities} from "../_demo-core/cities";
import {ListingsService} from "../api-services/listings.service";
import {finalize} from "rxjs/operators";
import {ActivatedRoute} from "@angular/router";

@Component({
	selector: 'app-listings',
	templateUrl: './listings.component.html',
	styleUrls: ['./listings.component.scss']
})
export class ListingsComponent implements OnInit {
	public filtersForm: FormGroup = new FormGroup({
		place_type: new FormControl([])
	});

	public priceFiltersForm: FormGroup = new FormGroup({
		min_price: new FormControl(null, Validators.required),
		max_price: new FormControl(null, Validators.required)
	});

	public locationName = "";
	public loading: boolean = true;
	public locations = cities;
	public start_date = null;
	public end_date = null;
	public count: number = null;
	public numberOfDays = null;
	public listings = [];
	public noResults: boolean;
	public expressions = {};
	public filters;

	constructor(
		private configService: ConfigService,
		private dialog: MatDialog,
		private listingService: ListingsService,
		private route: ActivatedRoute
	) {
	}

	ngOnInit(): void {
		this.route.queryParams.subscribe(res => {
			if(res) {
				this.expressions = {};
				if (res.type_of_place) {
					let expression = {query: res.type_of_place, operator: '=', column: 'listings.place_type'}
					this.expressions['place_type'] = this.configService.searchColumn([expression], true, false);
				}

				if (res.pets_allowed) {
					let expression = {query: true, operator: '=', column: 'listings.pets_allowed'}
					this.expressions['place_type'] = this.configService.searchColumn([expression], true, false);
				}

				if (navigator.geolocation && !this.configService.searchQuery.value) {
					this.locationName = "Near Me"
					navigator.geolocation.getCurrentPosition((position) => {
						const geoSearch: GeoSearchExpression = {
							column: 'listings.gps_coordinates',
							lat: position.coords.latitude,
							lng: position.coords.longitude,
							radius: 10000
						}
						this.expressions['location'] = this.configService.geoLocationSearch([geoSearch], false, false)[0];
						this.getListings();
					});
				}
			}
		});

		this.configService.searchQuery.asObservable().subscribe(query => {
			this.start_date = query?.start_at ? Date.parse(query.start_at) :  null;
			this.end_date = query?.end_at ?  Date.parse(query.end_at) : null;
			this.numberOfDays = Math.floor((this.end_date - this.start_date) / 86400000);

			if (query?.point) {
				const location = this.locations.find(x => x.latitude === query.point.data.lat && x.longitude === query.point.data.lng);
				this.locationName = location.city + ', ' + location.state;

				const geoSearch: GeoSearchExpression = {
					column: 'listings.gps_coordinates',
					lat: query.point.data.lat,
					lng: query.point.data.lng,
					radius: 10000
				}

				this.expressions['location'] = this.configService.geoLocationSearch([geoSearch], false, false)[0];

				this.getListings();
			}
		});
	}

	public openFiltersPanel() {
		const dialogRef = this.dialog.open(FiltersPanelComponent, {minWidth: '30vw', data: {filters: this.filters}});

		dialogRef.afterClosed().subscribe(res => {
			if (res) {
				this.filters = res;
				let amenitiesExpressions: ColumnSearchExpression[] = [];
				let facilitiesExpressions: ColumnSearchExpression[] = [];
				let expressions: ColumnSearchExpression[] = [];

				for (let x in res) {
					if (x === 'amenities') {
						for (let amenity in res[x]) {
							amenitiesExpressions.push({column: 'listings.amenities.' + amenity, query: true, or: false});
						}
					} else if (x === 'facilities') {
						for (let facility in res[x]) {
							facilitiesExpressions.push({column: 'listings.facilities.' + facility, query: true, or: false});
						}
					} else {
						if(x === 'occupancy' || x === 'bedrooms' || x === "bathrooms") {
							expressions.push({column: 'listings.' + x, query: res[x], or: false, operator: '>='});
						} else {
							expressions.push({column: 'listings.' + x, query: res[x], or: false});
						}

					}
				}

				this.expressions['expressions'] = this.configService.searchColumn(expressions, true, false);
				this.expressions['facilities'] = this.configService.searchColumn(facilitiesExpressions, true, false);
				this.expressions['amenities'] = this.configService.searchColumn(amenitiesExpressions, true, false);

				this.getListings();
			}
		});
	}

	public fourAmenities(amenities) {
		let fourAmenities = [];

		for (let x of amenities) {
			if (fourAmenities.length < 4 && x.value === true) {
				let keysplit = x.key.split('_')

				let amenity = keysplit[0] + ' ' + (keysplit[1] ? keysplit[1] : '')
				fourAmenities.push(amenity)
			}
		}
		return fourAmenities
	}

	public placeFilter(event, filter) {
		event.stopPropagation();

		if (this.filtersForm.controls.place_type.value.includes(filter)) {
			this.filtersForm.controls.place_type.patchValue(this.filtersForm.controls.place_type.value.filter(x => x !== filter))
		} else {
			this.filtersForm.controls.place_type.patchValue([filter, ...this.filtersForm.controls.place_type.value])
		}
	}

	public submitPlaceFilters() {
		let expressions: ColumnSearchExpression[] = [];

		for (let x of this.filtersForm.controls.place_type.value) {
			expressions.push({or: true, query: x, operator: 'includes', column: 'listings.place_type'});
		}

		this.expressions['place_type'] = this.configService.searchColumn(expressions, true, false);
		this.getListings();
	}

	getListings() {
		this.loading = true;
		this.listingService.listingsGet({expression: Object.values(this.expressions)}, this.start_date, this.end_date)
			.pipe(
				finalize(() => {
					this.loading = false;
					this.noResults = !!this.listings.length;
				})
			).subscribe(res => {
				this.listings = res?.items;
				this.count = res?.itemsTotal;
			});
	}

	public filterByPrice() {
		this.priceFiltersForm.markAllAsTouched();
		if(this.priceFiltersForm.valid) {
			let expressions: ColumnSearchExpression[] = [
				{column: 'listings.price', operator: '>', query: this.priceFiltersForm.controls.min_price.value},
				{column: 'listings.price', operator: '<',  query: this.priceFiltersForm.controls.max_price.value}
			];

			this.expressions['price'] = this.configService.searchColumn(expressions, true, false);

			this.getListings();
		}
	}
}
