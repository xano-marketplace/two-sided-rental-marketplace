import {Component, OnInit} from '@angular/core';
import {ListingsService} from "../../api-services/listings.service";
import {ActivatedRoute} from "@angular/router";
import {mergeMap} from "rxjs/operators";
import {MatDialog} from "@angular/material/dialog";
import {ReservationsPanelComponent} from "../../reservations-panel/reservations-panel.component";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ConfigService} from "../../_demo-core/config.service";

@Component({
	selector: 'app-listing',
	templateUrl: './listing.component.html',
	styleUrls: ['./listing.component.scss']
})
export class ListingComponent implements OnInit {

	public listing: any;
	public datesForm: FormGroup = new FormGroup({
		start_at: new FormControl('', Validators.required),
		end_at: new FormControl('', Validators.required)
	})

	constructor(
		private listingService: ListingsService,
		private configService: ConfigService,
		private route: ActivatedRoute,
		private dialog: MatDialog
	) {
	}

	ngOnInit(): void {
		this.route.params.pipe(mergeMap((params) => {
			return this.listingService.listingGet(params.id)
		})).subscribe(res => {
			this.listing = res;
		});

		this.configService.searchQuery.asObservable().subscribe(res => {
			if (res) {
				this.datesForm.patchValue(res)
			}
		})
	}

	public reserve(): void {
		if (this.datesForm.valid) {
			this.dialog.open(ReservationsPanelComponent, {
				data: {
					listing: this.listing,
					dates: this.datesForm.getRawValue()
				}
			})
		}
	}


}
