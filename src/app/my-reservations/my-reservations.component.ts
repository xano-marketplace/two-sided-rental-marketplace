import {Component, OnInit} from '@angular/core';
import {ConfigService} from "../_demo-core/config.service";
import {ReservationsService} from "../api-services/reservations.service";
import {finalize} from "rxjs/operators";

@Component({
	selector: 'app-my-reservations',
	templateUrl: './my-reservations.component.html',
	styleUrls: ['./my-reservations.component.scss']
})
export class MyReservationsComponent implements OnInit {

	constructor(
		private configService: ConfigService,
		private reservationsService: ReservationsService
	) {
	}

	public reservations;
	public noReservations: boolean;

	ngOnInit(): void {
		this.reservationsService.myReservationsGet(null)
			.pipe(finalize(() => this.noReservations = !this.reservations?.length))
			.subscribe(res => {
				this.reservations = res?.items;
			}, error => this.configService.showErrorSnack(error))
	}

	public cancel(reservation) {
		reservation.canceled_at = new Date()
		this.reservationsService.reservationsUpdate(reservation).subscribe(res => {
			if(res) {
				this.reservations.map(x => {
					if(x.id === reservation.id) {
						x = reservation;
					}
					return x;
				})
			}
		},error => this.configService.showErrorSnack(error))
	}
}
